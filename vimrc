filetype plugin indent on
syntax enable
set et sts=2 ts=2 sw=2 backspace=2 hidden confirm rnu nu showcmd cul
set lcs=eol:$,tab:›-,space:·,trail:•
let g:solarized_termcolors=16
set background=dark
colorscheme solarized
let g:vim_airline_theme='solarized'
let g:arline_solarized_bg='dark'
let g:airline_powerline_fonts = 1
let g:airline#extensions#ale#enabled = 1
set rtp+=/usr/local/opt/fzf
set pythonthreedll=/usr/local/Frameworks/Python.framework/Versions/Current/Python


let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
set omnifunc=ale#completion#OmniFunc

let g:limelight_conceal_ctermfg = 10

hi CursorLineNr ctermfg=LightYellow cterm=bold gui=bold guifg=LightYellow

set fdm=syntax
set grepprg=grep\ -nH\ $*
nnoremap <space> i <esc>r
au BufEnter * silent! lcd %:p:h
au FileType qf wincmd J 
let g:tex_flavor='latex'

if has('persistent_undo')      "check if your vim version supports it
  set undofile                 "turn on the feature
  set undodir=$HOME/.vim/undo  "directory where the undo files will be stored
endif

set wildignore+=*.pdf,*.log,*.toc,*.synctex,*.synctex.gz,*.fls,*.aux,*.blg
set suffixes+=.bbl,.bib,.bst,.cls,.log,.aux,.idx,.ind,.out,.toc,.dvi,.fdb_latexmk
set hls

call togglebg#map("<F12>")
