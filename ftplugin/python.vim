let b:ale_fixers = ['black', 'isort']
let b:ale_linters = ['pylsp']
let b:ale_python_pylsp_config = {
      \ 'pylsp': {
      \   'plugins': {
      \     'pydocstyle': {
      \       'enabled': v:true
      \       },
      \     'mypy': {
      \       'enabled': v:true
      \       }
      \     }
      \   },
      \ }

" augroup HoverAfterComplete
"   autocmd!
"   autocmd User ALECompletePost ALEHover
" augroup END
