let g:Tex_DefaultTargetFormat = 'pdf'
let g:Tex_ViewRule_pdf = 'open -a Skim.app'
let g:Tex_TreatMacViewerAsUNIX = 1
let b:ale_linters = ['texlab'] 
setl omnifunc=ale#completion#OmniFunc
