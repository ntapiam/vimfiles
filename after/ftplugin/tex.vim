setl ts=2 sw=2 sts=2 noet tw=150 ai fo+=lr
setl iskeyword+=:
let g:Tex_MultipleCompileFormats .= ',pdf'
let Tex_FoldedEnvironments .= ',thm,crl,lmm,dfn,prp,proof,xmp,theorem,corollary,definition,proposition,example,lemma'
imap <C-L> <Plug>Tex_LeftRight
imap <C-K> <Plug>IMAP_JumpBack
function! Tex_ForwardSearchLaTeX()
  let filename = fnamemodify(Tex_GetMainFileName(), ":p:r")
  let cmd = '/usr/local/bin/displayline -r ' . line(".") . ' "' . filename .  '.pdf" ' . '"' . expand("%:p") . '"'
  let output = system(cmd)
endfunction

" custom mappings
ino <buffer> ,, ,\dotsc,
ino <buffer> ++ +\dotsb+
ino <buffer> << <\dotsb<
ino <buffer> <= \le
ino <buffer> ≤ \le
ino <buffer> >> >\dotsb>
ino <buffer> >= \ge
ino <buffer> ≥ \ge
ino <buffer> ∑ \sum_{<++>}^{<++>}

